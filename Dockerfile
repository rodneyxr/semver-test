FROM docker.io/library/busybox:1.35.0
ARG VERSION=0.0.0
LABEL nothing.version=${VERSION}
ENV VERSION=$VERSION
ENTRYPOINT echo v${VERSION}